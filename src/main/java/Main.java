import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {
	private static List<Coin> listOfCoins = new ArrayList<Coin>();

	public static void main(String[] args) {
		String url = "https://finance.yahoo.com/cryptocurrencies?count=150";
		Document doc = null;

		try {
			doc = Jsoup.connect(url).get();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (doc == null) {
			System.out.println("failed to get website");
			System.exit(1);
		}

		Elements headers = doc.select("#scr-res-table table > thead th > span");
		if (headers.eachText().size() < 10 || headers.eachText().size() > 12) {
			System.out.println("Expected between 10 and 12 headers, but found " + headers.eachText().size());
			System.exit(1);
		} else {
			System.out.println("Properties: " + headers.eachText());
		}

		Elements symbol_pairs = doc.select("#scr-res-table table > tbody > tr > td > a");
		if (symbol_pairs.eachText().size() < 100) {
			System.out.println("Expected at least 100 symbol pairs, but found " + symbol_pairs.eachText().size());
			System.exit(1);
		} else {
			System.out.println("Symbol Pairs: " + symbol_pairs.eachText());
		}

		for (Element row : doc.select("#scr-res-table table > tbody > tr")) {
			Coin newCoin = new Coin(row);
			System.out.println(newCoin.toString());
			listOfCoins.add(newCoin);
		}

		if (Snapshot.commitSnapshot(listOfCoins))
			System.out.println("\n\tSnapshot Successfully Committed to DB");
		else
			System.out.println("\n\tFailed to Push to database");
	}
}
