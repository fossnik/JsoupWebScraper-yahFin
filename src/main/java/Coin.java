import java.lang.reflect.Field;
import java.security.InvalidParameterException;
import org.jsoup.nodes.Element;

public class Coin {
	private String iconUrl;
	private String exchangePair;
	private String symbolFullName;
	private Double price;
	private Double change;
	private Double pChange;
	private Double marketCap;
	private Double volume;
	private Double volume24h;
	private Double totalVolume24h;
	private Double circulatingSupply;

	Coin(Element row) {
		this.setIconUrl(row);
		this.setExchangePair(row);
		this.setSymbolFullName(row);
		this.setPrice(row);
		this.setChange(row);
		this.setpChange(row);
		this.setMarketCap(row);
		this.setVolume(row);
		this.setVolume24h(row);
		this.setTotalVolume24h(row);
		this.setCirculatingSupply(row);
	}

	@Override
	public String toString() {
		StringBuilder remit = new StringBuilder("Coin{ ");

		for (Field field : this.getClass().getDeclaredFields())
			try {
				if (field.getType().toString().contains("class java.lang."))
					remit.append(field.getName()).append("=").append(field.get(this)).append(" ");
			} catch (IllegalAccessException e) {
				System.out.println("IllegalAccessException - " + field.getName());
			}

		return remit.toString() + '}';
	}

	String getIconUrl() {
		return iconUrl;
	}

	String getExchangePair() {
		return exchangePair;
	}

	String getSymbolFullName() {
		return symbolFullName;
	}

	Double getPrice() {
		return price;
	}

	Double getChange() {
		return change;
	}

	Double getpChange() {
		return pChange;
	}

	Double getMarketCap() {
		return marketCap;
	}

	Double getVolume() {
		return volume;
	}

	Double getVolume24h() {
		return volume24h;
	}

	Double getTotalVolume24h() {
		return totalVolume24h;
	}

	Double getCirculatingSupply() {
		return circulatingSupply;
	}

	private void setIconUrl(Element row) {
		this.iconUrl = row.getElementsByTag("img").attr("src");
	}

	private void setExchangePair(Element row) {
		this.exchangePair = row.child(0).text();
		if (!this.exchangePair.contains("-")) {
			System.out.println("Implementation Fracture: set Exchange Pair");
			System.exit(1);
		}
	}

	private void setSymbolFullName(Element row) {
		this.symbolFullName = row.child(1).text();
	}

	private void setPrice(Element row) {
		this.price = Double.valueOf(row.child(2).text().replaceAll(",", ""));
	}

	private void setChange(Element row) {
		this.change = Double.valueOf(row.child(3).text().replaceAll(",", ""));
	}

	private void setpChange(Element row) {
		String input = row.child(4).text();

		if (input.contains("%")) {
			this.pChange = Double.valueOf(input.replaceAll("%", "").replaceAll(",", ""));
		} else {
			System.out.println("Implementation Fracture: set Percent Change");
			System.exit(1);
		}
	}

	private void setMarketCap(Element row) {
		this.marketCap = parseMagnitude(row.child(5).text());
	}

	private void setVolume(Element row) {
		this.volume = parseMagnitude(row.child(6).text());
	}

	private void setVolume24h(Element row) {
		this.volume24h = parseMagnitude(row.child(7).text());
	}

	private void setTotalVolume24h(Element row) {
		this.totalVolume24h = parseMagnitude(row.child(8).text());
	}

	private void setCirculatingSupply(Element row) {
		this.circulatingSupply = parseMagnitude(row.child(9).text());
	}

	private static double parseMagnitude(String s) {
		String string = s.replaceAll("[^0-9.MBT]", "");

		// M B and T for Millions, Billions, and Trillions. (eg 142.43B	=== 142,000,000,000)
		switch (string.charAt(string.length() - 1)) {
			case 'M':
				return Double.valueOf(string.replaceAll("M", "")) * 1000000D;
			case 'B':
				return Double.valueOf(string.replaceAll("B", "")) * 1000000000D;
			case 'T':
				return Double.valueOf(string.replaceAll("T", "")) * 1000000000000D;
			default:
				if (string.matches("[\\D.]+"))
					throw new InvalidParameterException("Magnitude Conversion Failure - Invalid Non-digit Characters");
				else
					return Double.valueOf(string);
		}
	}
}
